<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStockdetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stockdetails', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('company_name')->nullable();
            $table->string('category');
            $table->string('price')->nullable();
            $table->string('target')->nullable();
            $table->string('stoploss')->nullable();
            $table->string('company_logo')->nullable();
            $table->string('image_chart')->nullable();
            $table->string('stock_state');
            $table->text('description')->nullable();
            $table->string('status');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stockdetails');
    }
}
