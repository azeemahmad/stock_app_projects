@extends('admin.layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-success">
                    <div class="panel-heading">Stockdetails</div>
                    <div class="panel-body">
                        <a href="{{ url('/admin/stockdetails/create') }}" class="btn btn-success btn-sm"
                           title="Add New Stockdetail">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>

                        {!! Form::open(['method' => 'GET', 'url' => '/admin/stockdetails', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id="data-table">
                                <thead>
                                <tr>
                                    <th>Sr.no</th>
                                    <th>Company Name</th>
                                    <th>Category</th>
                                    <th>Price</th>
                                    <th>Target</th>
                                    <th>Stoploss</th>
                                    <th>Company Logo</th>
                                    <th>Image Chart</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($stockdetails as $key => $item)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $item->company_name }}</td>
                                        <td>{{ $item->category }}</td>
                                        <td>{{ $item->price }}</td>
                                        <td>{{ $item->target }}</td>
                                        <td>{{ $item->stoploss }}</td>
                                        <td><img src="{{asset('CompanyLogo/'.$item->company_logo)}}"
                                                    height="40px" width="100px"></td>
                                        <td><img src="{{asset('ImageChart/'.$item->image_chart)}}"
                                                 height="100px" width="100px"></td>
                                        <td>
                                            <a href="{{ url('/admin/stockdetails/' . $item->id) }}"
                                               title="View Stockdetail">
                                                <button class="btn btn-info btn-xs"><i class="fa fa-eye"
                                                                                       aria-hidden="true"></i> View
                                                </button>
                                            </a>
                                            <a href="{{ url('/admin/stockdetails/' . $item->id . '/edit') }}"
                                               title="Edit Stockdetail">
                                                <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"
                                                                                          aria-hidden="true"></i> Edit
                                                </button>
                                            </a>

                                            <form method="POST"
                                                  action="{{ url('/admin/stockdetails' . '/' . $item->id) }}"
                                                  accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-xs"
                                                        title="Delete Stockdetail"
                                                        onclick="return confirm(&quot;Confirm delete?&quot;)"><i
                                                            class="fa fa-trash-o" aria-hidden="true"></i> Delete
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $stockdetails->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
