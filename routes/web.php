<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('client.index');
});






/******************  START  CLIENT ROUTE::    ***********************************************/

Route::group(['prefix' => 'client', 'namespace' => 'Client\Auth', 'middleware' => 'client_guest'], function () {
    Route::get('/register', 'RegisterController@showRegistrationForm')->name('client.register');
    Route::post('/register', 'RegisterController@register')->name('client.register');
    Route::get('/login', 'LoginController@showLoginForm')->name('client.login');
    Route::post('/login', 'LoginController@postlogin')->name('client.login');
    Route::get('/password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('client.password.request');
    Route::post('/password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('client.password.email');
    Route::get('/password/reset/{id}', 'ResetPasswordController@showResetForm')->name('client.password.reset');
    Route::post('/password/reset/{id}', 'ResetPasswordController@reset')->name('client.password.update');
    Route::get('/mobile_verification','ClientController@mobile_verification');
    Route::post('/otp_verification','ClientController@otp_verification');
});

Route::group(['prefix' => 'client', 'namespace' => 'Client', 'middleware' => 'client_auth'], function () {
    Route::get('/logout', 'Auth\LoginController@logout')->name('client.logout');
    Route::get('/home', 'HomeController@index');
    Route::get('/profile', 'HomeController@profile');
    Route::post('/profile', 'HomeController@saveprofile');
});
/******************  END  CLIENT ROUTE::    ***********************************************/


/******************  START ADMIN ROUTE::    ***********************************************/
Route::group(['prefix' => 'admin','namespace' => 'Admin\Auth','middleware' => 'guest'], function () {
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login');
    Route::get('register', 'RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'RegisterController@register');
    Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'ResetPasswordController@reset')->name('password.update');
});

Route::group(['prefix' => 'admin','namespace' => 'Admin\Auth','middleware' => 'auth'], function () {
    Route::get('email/verify', 'VerificationController@show')->name('verification.notice');
    Route::get('email/verify/{id}', 'VerificationController@verify')->name('verification.verify');
    Route::get('email/resend', 'VerificationController@resend')->name('verification.resend');
    Route::get('/logout', 'LoginController@logout')->name('admin.logout');
});
Route::group(['prefix' => 'admin','namespace' => 'Admin','middleware' => ['auth', 'verified']], function () {
    Route::get('/home', 'HomeController@index');
    Route::get('/profile', 'HomeController@profile');
    Route::post('/profile', 'HomeController@saveprofile');
    Route::resource('/users', 'UserController');
    Route::resource('/roles', 'RoleController');
    Route::resource('stockdetails', 'StockdetailsController');
});
/******************  END ADMIN ROUTE::    ***********************************************/






